<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link type="text/css" rel="stylesheet" href="public/assets/css/main.css">
    <!-- <link type="text/css" rel="stylesheet" href="src/styles/style.css"> -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <script type="module" src="public/assets/scripts/main.js"></script>
    <title><?= $title;?></title>
</head>
<body>
<header>
  <div class="header_main">
    <div class="logo">
      <a class="lien" href="./index.php">
        <img class="icon-head" src="../public/medias/icones/simplonco.svg" alt="">
      </a>
      <h1 class="title">simplon.comp</h1>
    </div>
    <h2 class="title">L’appli interne des compétences formateurs.</h2>
    <a class="connexion" href="#">
      <img class="icon_co" src="../public/medias/icones/user.svg" alt="">
      <p>connexion</p>
    </a>
  </div>

  <?php 
  $current = explode("|", $title);
  $back = $_SERVER["HTTP_REFERER"] ?? null;
  ?>
  <div class="breadcrumbs">
  <?php if($back):?>
    <a href= <?=$back;?>>retour</a>
  <?php endif;?>
  <p><?= $current[1];?></p>
    <a href="#">breadcrumbs</a>
  </div>
</header>