<?php 
$title = 'simplon.comp | recherche2';
include "partials/header.php";
?>
<main id="research_page" class="research_page">
  <div class="background">
    <div>
      <h1>Sélectionnez un critère</h1>
    </div>
    <div class="drop-down-menu">
      <!-- Teachers part-->
      <div class="choices">
        <img class="icon" src="public/medias/icones/teacher.svg"/>
        <span>Rechercher un formateur</span>
        <button class="arrow-button arrow">
          <img src="public/medias/icones/arrow_black.svg" alt="Voir">
        </button>
      </div>
      <ul id="teachers" class="teachers-list options list" data-class="teacher">
      </ul>

      <!-- Loc part-->
      <div class="choices">
        <img class="icon" src="public/medias/icones/map.svg"/>
        <span>Rechercher une localisation</span>
        <button class="arrow-button arrow">
          <img src="public/medias/icones/arrow_black.svg" alt="Voir">
        </button>
      </div>
      <ul id="loc" class="loc-list options list" data-class="loc">
        <!-- <li class="thumbnail" data-target="7">
          Localisation 1
        </li>
        <li class="thumbnail" data-target="8">
          Localisation 2
        </li>
        <li class="thumbnail" data-target="9">
          Localisation 3
        </li> -->
      </ul>

      <!--Skills part-->
      <div class="choices">
        <img class="icon" src="public/medias/icones/tool.svg"/>
        <span>Rechercher une compétence</span>
        <button class="arrow-button arrow">
          <img src="public/medias/icones/arrow_black.svg" alt="Voir">
        </button>
      </div>
      <div class="options skills-dropdown">
        <div class="drop-down-menu">
          <!-- Family-->
          <div class="choices family">
            <span >Famille</span>
            <button class="arrow-button arrow">
              <img src="public/medias/icones/arrow_black.svg" alt="Voir">
            </button>
          </div>
          <ul class="family-list options list" data-class="family">
            <li class="family thumbnail" data-target="10">
              Famille 1
            </li>
            <li class="family thumbnail" data-target="11">
              Famille 2
            </li>
            <li class="family thumbnail" data-target="12">
              Famille 3
            </li>
            <li class="family thumbnail" data-target="13">
              Famille 4
            </li>
          </ul>

          <!-- Sub-family-->
          <div class="choices sub-fam">
            <span >Sous-famille</span>
            <button class="arrow-button arrow">
              <img src="public/medias/icones/arrow_black.svg" alt="Voir">
            </button>
          </div>
          <ul class="sub-fam-list options list" data-class="sub-fam">
            <li class="thumbnail" data-target="14">
              Sous-famille 1
            </li>
            <li class="thumbnail" data-target="15">
              Sous-famille 2
            </li>
            <li class="thumbnail" data-target="16">
              Sous-famille 3
            </li>
          </ul>

          <!--Skills-->
          <div class="choices skill">
            <span >Compétence</span>
            <button class="arrow-button arrow">
              <img src="public/medias/icones/arrow_black.svg" alt="Voir">
            </button>
          </div>
          <ul class="skills-list options list" data-class="skill">
            <li class="thumbnail" data-target="17">
              Compétence 1
            </li>
            <li class="thumbnail" data-target="18">
              Compétence 2
            </li>
            <li class="thumbnail" data-target="19">
              Compétence 3
            </li>
          </ul>

          <!--Level-->
          <div class="choices level">
            <span >Niveau</span>
            <button class="arrow-button arrow">
              <img src="public/medias/icones/arrow_black.svg" alt="Voir">
            </button>
          </div>
          <ul class="level-list options list" data-class="level">
            <li class="thumbnail" data-target="20">
              <img class="icon" src="public/medias/icones/triforce_niv1.svg" alt="">
              Débutant
            </li>
            <li class="thumbnail" data-target="21">
              <img class="icon" src="public/medias/icones/triforce_niv2.svg" alt="">
              Junior
            </li>
            <li class="thumbnail" data-target="22">
              <img class="icon" src="public/medias/icones/triforce_niv3.svg" alt="">
              Confirmé
            </li>
            <li class="thumbnail" data-target="23">
              <img class="icon" src="public/medias/icones/triforce_niv4.svg" alt="">
              Expert
            </li>
          </ul>                                  
        </div>
      </div>
    
      <!--Jobs part-->
      <div class="choices">
        <img class="icon" src="public/medias/icones/job.svg"/>
        <span>Rechercher un référentiel métier</span>
        <button class="arrow-button arrow">
          <img src="public/medias/icones/arrow_black.svg" alt="Voir">
        </button>
      </div>       
      <ul id="jobs" class="job-list options list" data-class="job">
    </div>
    <!--Selection part-->
    <div id="fil" class="tools">
      <div class="icon-selected">
        <img class="icon-form" src="public/medias/icones/versions.svg" alt="">
      </div>
      <ul id="ariane" class="selected"></ul>
    </div>
    <div class="button-inner-style" id="research-button">
      <button class="general-button" type="submit">
        <span>rechercher</span>
        <svg class="icon-button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" x="0px" y="0px"><path d="M278.849165,27.8491646 L285.290053,34.2900534 C285.676482,34.6764822 285.681783,35.3182165 285.289243,35.7107565 L284.710757,36.2892435 C284.3125,36.6875 283.682146,36.6821461 283.290053,36.2900534 L276.849165,29.8491646 C274.437384,31.8187043 271.356645,33 268,33 C260.268014,33 254,26.7319865 254,19 C254,11.2680135 260.268014,5 268,5 C275.731986,5 282,11.2680135 282,19 C282,22.3566452 280.818704,25.437384 278.849165,27.8491646 Z M268,31 C274.627417,31 280,25.627417 280,19 C280,12.372583 274.627417,7 268,7 C261.372583,7 256,12.372583 256,19 C256,25.627417 261.372583,31 268,31 Z" transform="translate(-250)"/></svg>
      </button>
    </div>
  </div>
</main>
<?php include "partials/footer.php"; ?>