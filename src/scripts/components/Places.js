import AdvancedResearch from './AdvancedResearch.js'
import Filters from './Filters.js'

export default class Places {
  constructor(data) {
    this.parent = document.getElementById('loc')
    this.idPrefix = 'place'

    this.places = data.places.countries
    this.extractDatas()

    Filters.displayAlphaFilters(this.places, this.parent, this.idPrefix)
    AdvancedResearch.displayDatas(this.places, this.parent, this.idPrefix)
  }

  /**
   * Function to separate the different data types
   */
  extractDatas() {
    this.places.forEach((country) => {
      if (country.regions) {
        country.regions.forEach(region => {
          this.places.push(region)
          if (region.cities) {
            region.cities.forEach(city => {
              this.places.push(city)
            })
          }
        })
      }
    })
    this.places = this.sortData(this.places)
    this.places.type = 'place'
  }

  sortData(datas) {
    datas.sort((a, b) => (a.name > b.name) ? 1 : -1);
    return datas
  }
}