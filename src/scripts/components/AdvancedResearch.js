import Teachers from "./Teachers.js"
import Places from "./Places.js"
import App from "../App.js"
import Filters from "./Filters.js"

export default class AdvancedResearch {
  constructor(data) {
    this.arrows = document.querySelectorAll('.arrow')

    this.eventListeners()
    this.teachers = new Teachers(data)
    this.places = new Places(data)
  }

  /*Function to display research options according the datas from the json file*/
  static displayDatas(datas, parent, idPrefix) {
    let index = 0
    datas.forEach((data) => {
      let text = data.name
      AdvancedResearch.createLi(text, parent, idPrefix, index)
      index++
    })
  }
  
  /*Function to create li tags with the research options*/ 
  static createLi(text, parent, idPrefix, index) {
    let classToAdd = parent.dataset.class
    let liItem = document.createElement('li')
    liItem.addEventListener('click', AdvancedResearch.select)
    liItem.classList.add('thumbnail', classToAdd)
    liItem.dataset.target = idPrefix + index
    let liContent = document.createTextNode(text)
    liItem.appendChild(liContent)
    parent.appendChild(liItem)
  }

  /*Function to select the thumbnails for research*/
  static select(e) {
    let ariane = document.getElementById('ariane')
    let thumb = e.target
    let target_id = 'target-' + thumb.dataset.target


    if (!thumb.classList.contains('active')) {
      let text = thumb.innerText
      let new_thumb = document.createElement('li')
      new_thumb.appendChild(document.createTextNode(text))
      new_thumb.id = target_id
      let cross = document.createElement('img')
      cross.setAttribute('src', '/public/medias/icones/close.svg')
      cross.className = "close"
      new_thumb.appendChild(cross)
      cross.addEventListener('click', () => { App.del(thumb) })
      new_thumb.classList.add('thumbnail', thumb.parentNode.dataset.class)
      ariane.appendChild(new_thumb)
      thumb.classList.add('active')
    } else {
      thumb.classList.remove('active')
      document.getElementById(target_id).remove()
    }
  }

  /*Function to add event listeners to toggle the research options*/
  eventListeners() {
    this.arrows.forEach(arrow => {
      arrow.addEventListener('click', App.addresearch)
    })
  }

}