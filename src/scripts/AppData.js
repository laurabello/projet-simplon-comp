export default class AppData {

  static getData(file, cb) {
    return fetch(file)
      .then(response => {
        if (response.status !== 200) {
          console.log('Looks like there was a problem. Status Code: ' +
            response.status)
          return
        }
        response.json().then(data => {
          cb(data)
        });
      })
      .catch(err => {
        console.log('Fetch Error', err)
      });
  }
}