import AppData from './AppData.js'
import AdvancedResearch from './components/AdvancedResearch.js'
import Home from './components/Home.js'

export default class App {
  constructor (){
    this.jsonFile = '../public/datas/datas.json'
    this.loadData()

  }

  /*function to load the datas in the json file and initiate the main components*/
  loadData() {
    if (document.getElementById('research_page')) {
      AppData.getData(this.jsonFile, data => {
        new AdvancedResearch(data)
      })
    } else {
      new Home()
    }
  }

  /*Function to toggle elements on click*/
  static addresearch(e) {
    let drop = e.currentTarget.parentNode.nextElementSibling
    let arrow = (e.currentTarget).querySelector('img')
    let arrows = document.querySelectorAll('.arrow>img')
    let options = document.querySelectorAll('.options')

    if (drop.style.display != "flex") {
      drop.style.display = "flex"
      arrow.classList.add('toggled')

      options.forEach(option => {
        if (option != drop && option.style.display == "flex") {
          option.style.display = "none"
        }
      })

      arrows.forEach(a => {
        if (a != arrow && a.classList.contains('toggled')) {
          a.classList.remove('toggled')
        }
      })

    } else {
      drop.style.display = "none"
      arrow.classList.remove('toggled')
    }
  }

  /*Function to delete thumbnails*/
  static del(e) {
    let cl = event.target

    if (document.getElementById('research_page')) {
      ariane.removeChild(cl.parentNode)
      e.classList.remove('active')
    } else {
      cl.parentNode.parentNode.removeChild(cl.parentNode)
    }
  }
}