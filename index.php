<?php 
$title = 'simplon.comp | home page';
include "partials/header.php";
?>
  <main class="index_page">
  <!--Search section-->
  <section class="research_section">
    <form class="text_research">
      <div class="research-img">
        <label for="research">
          <img class="icon" src="public/medias/icones/zoom.svg" alt="">
          <span>Recherche</span>
        </label>
      </div>
      <div class="research-input">
        <input id="research" type="text" name="research" placeholder="Votre recherche">
      </div>
      <ul id="select-list" class="select-list"></ul>
      <div class="button-inner-style" id="research-button">
        <button class="general-button" type="submit">
          <span>rechercher</span>
          <svg class="icon-button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" x="0px" y="0px"><path d="M278.849165,27.8491646 L285.290053,34.2900534 C285.676482,34.6764822 285.681783,35.3182165 285.289243,35.7107565 L284.710757,36.2892435 C284.3125,36.6875 283.682146,36.6821461 283.290053,36.2900534 L276.849165,29.8491646 C274.437384,31.8187043 271.356645,33 268,33 C260.268014,33 254,26.7319865 254,19 C254,11.2680135 260.268014,5 268,5 C275.731986,5 282,11.2680135 282,19 C282,22.3566452 280.818704,25.437384 278.849165,27.8491646 Z M268,31 C274.627417,31 280,25.627417 280,19 C280,12.372583 274.627417,7 268,7 C261.372583,7 256,12.372583 256,19 C256,25.627417 261.372583,31 268,31 Z" transform="translate(-250)"/></svg>
        </button>
      </div>

    </form>
      <a href="recherche.php" class="subtitles add-research"><img class="icon" src="public/medias/icones/plus.svg" alt="">Recherche avancée</a>
  </section>

  <!--Map section-->
  <section class="map_section">
    <a href="#" alt="accéder à la carte des compétences" class="map">
      <span class="subtitles">Acccéder à la carte des compétences</span>
    </a>
  </section>
</main>
<?php include "partials/footer.php"; ?>