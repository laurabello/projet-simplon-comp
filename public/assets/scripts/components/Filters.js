import AdvancedResearch from './AdvancedResearch.js'

export default class Filters {

  /**
   * Function to add the alphabetical filter
   * @param {Object} datas Set of datas for the current dropdown menu
   * @param {Dom element} parent Ul parent to display the dropdown
   * @param {Sting} idPrefix Prefix to get different ids according the type of data
   */
  static displayAlphaFilters(datas, parent, idPrefix) {
    let filtersDiv = document.createElement('div')
    filtersDiv.classList.add('filter')
    let container = document.createElement('li')

    //Add 'check all' radio button first
    //the input
    let checkAll = document.createElement('input')
    checkAll.name = idPrefix + '_' + 'AlphaFilter'
    checkAll.id = idPrefix + '_' + 'allAlpha'
    checkAll.type = 'radio'
    checkAll.value = 'all'
    checkAll.addEventListener('change', () => {
      Filters.executeAlphaFilters(datas, parent, idPrefix)
    })

    //the label
    let labelAll = document.createElement('label')
    labelAll.htmlFor = idPrefix + 'allAlpha'
    labelAll.innerText = 'Tous'

    parent.appendChild(filtersDiv)
    filtersDiv.appendChild(container)
    container.appendChild(checkAll)
    container.appendChild(labelAll)

    //Then add a radio button for each letter of the alphabet
    let i = 0
    while (i < 26) {
      let UpLetter = String.fromCharCode(65 + i)
      let lowLetter = String.fromCharCode(97 + i)
      let container = document.createElement('li')

      //the input
      let check = document.createElement('input')
      check.name = idPrefix + '_AlphaFilter'
      check.id = idPrefix + '_' + lowLetter
      check.type = 'radio'
      check.value = lowLetter
      check.addEventListener('change', () => {
        Filters.executeAlphaFilters(datas, parent, idPrefix)
      })

      //the label
      let label = document.createElement('label')
      label.htmlFor = idPrefix + '_' + lowLetter
      label.innerText = UpLetter

      parent.appendChild(filtersDiv)
      filtersDiv.appendChild(container)
      container.appendChild(check)
      container.appendChild(label)

      i++
    }
  }


  /**
   * Function to execute the alphabetical filter on change on radio button
   * @param {Object} datas Set of datas for the current dropdown menu
   * @param {Dom element} parent Ul parent to display the dropdown
   * @param {Sting} idPrefix Prefix to get different ids according the type of data
   * Those parameters are passed when adding the event listener on the radio buttons
   */
  static executeAlphaFilters(datas, parent, idPrefix) {
    let checkbox = event.target
    let dataDisplayed = Array.from(parent.getElementsByClassName('thumbnail'))

    if (checkbox.value == 'all' && checkbox.checked) {
      dataDisplayed.forEach(e => {
        parent.removeChild(e)
      })
      AdvancedResearch.displayDatas(datas, parent, idPrefix)
    } else if (checkbox.checked) {
      if (datas.type === 'teacher') {
        datas = datas.filter(data => ((data.lastName).charAt(0)) == (checkbox.value.toUpperCase()))
      } else {
        datas = datas.filter(data => ((data.name).charAt(0)) == (checkbox.value.toUpperCase()))
      }
      dataDisplayed.forEach(e => {
        parent.removeChild(e)
      })
      AdvancedResearch.displayDatas(datas, parent, idPrefix)
    } else {
      dataDisplayed.forEach(e => {
        parent.removeChild(e)
      })
      AdvancedResearch.displayDatas(datas, parent, idPrefix)
    }
  }

}