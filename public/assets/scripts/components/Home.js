import App from '../App.js'

export default class Home {
  constructor() {
    this.inputThumb = document.getElementById('select-list')
    this.homeEventListeners()
  }

  static select_up(e) {
    let input = document.getElementById('research')
    let research = document.getElementById('select-list')

    let text = input.value
    if (text != "") {
      let new_thumb = document.createElement('li')
      new_thumb.appendChild(document.createTextNode(text))

      let cross = document.createElement('img')
      cross.setAttribute('src', '/public/medias/icones/close.svg')
      cross.className = "close"

      new_thumb.appendChild(cross)
      research.appendChild(new_thumb)
      input.value = ""
      e.preventDefault()
    }
  }

  homeEventListeners() {
    document.getElementById('research-button').addEventListener('click', Home.select_up)
    if (this.inputThumb) {
      this.inputThumb.addEventListener('click', App.del)
    }
  }
}