import AdvancedResearch from "./AdvancedResearch.js"
import Filters from './Filters.js'

export default class Teachers {
  constructor(data) {
    this.parent = document.getElementById('teachers')
    this.datas = data.teachers
    this.sortedDatas = this.sortdata(this.datas)
    this.idPrefix = 'teach'

    Filters.displayAlphaFilters(this.sortedDatas, this.parent, this.idPrefix)
    AdvancedResearch.displayDatas(this.sortedDatas, this.parent, this.idPrefix)
  }

  /**
   * Function to sort the names alphabetically
   * @param {Object} datas Teachers entry from the json datas
   */
  sortdata(datas) {
    datas.forEach(data => {
      let name = (data.name).split(' ')
      data.firstName = name[0]
      data.lastName = name[1]
    })
    datas.sort((a, b) => (a.lastName > b.lastName) ? 1 : -1);
    datas.type = "teacher"
    return datas
  }
} 