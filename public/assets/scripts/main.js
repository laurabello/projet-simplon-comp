import App from './App.js';

function ready(fn) {
  if (document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}
ready(() => {
  getApp();
});

function getApp() {
  if (!window.app) {
    const app = new App();
    window.app = app;
  }
  return window.app = app;
}